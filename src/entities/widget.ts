import { sql } from '../lib/db'

import logger from '../lib/logger'
import { on } from '../lib/utils'

type Widget = {
  id: string
  name: string
}

export const listWidgets = async (): Promise<Widget[]> => {
  const [err, widgets] = await on(sql<Widget[]>`
    SELECT
      id,
      name
    FROM
      widgets
  `)

  if (err) {
    logger.error(err)
    throw err
  }

  return widgets as Widget[]
}

export const findWidget = async (id: string): Promise<Widget | null> => {
  const [err, widgets] = await on(sql<Widget[]>`
    SELECT
      id,
      name
    FROM
      widgets
    WHERE
      id = ${id}
  `)

  if (err) {
    logger.error(err)
    throw err
  }

  const w = widgets as Widget[]

  if (w[0]) {
    return w[0]
  }

  return null
}
