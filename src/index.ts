import './lib/config' // must be imported before everything else

import pino from 'pino'

import db from './lib/db'
import httpServer from './lib/http-server'
import logger from './lib/logger'

// app entry-point
;(async () => {
  await db.connect()
  await httpServer.start()
})()

process.on(
  'SIGINT',
  pino.final(logger, async (_, finalLogger) => {
    // The first argument to this function seems to always be 'SIGINT'
    finalLogger.warn('Caught SIGINT.')
    await httpServer.stop()
    await db.disconnect(finalLogger)
    finalLogger.info('Quitting, bye bye.')
    process.exit(0)
  }),
)

process.on(
  'unhandledRejection',
  pino.final(logger, (err, finalLogger) => {
    finalLogger.error(err)
    process.exit(1)
  }),
)

process.on(
  'unhandledException',
  pino.final(logger, (err, finalLogger) => {
    finalLogger.error(err)
    process.exit(1)
  }),
)
