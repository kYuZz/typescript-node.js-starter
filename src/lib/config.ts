import 'dotenv/config'

type AppEnvironment = 'development' | 'staging' | 'production'

const envVarError = (envVar: string, message?: string) => {
  let errorMsg
  if (!message) {
    errorMsg = `Environment variable ${envVar} not set.`
  } else {
    errorMsg = `${envVar}: ${message}`
  }
  process.stderr.write(`${errorMsg}\n`)
  process.exit(1)
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type EnvVarProcessor = (val?: string) => any

const getEnvVarProcessor = (envVar: string): EnvVarProcessor | undefined => {
  if (envVar === 'NODE_ENV') {
    return (val?: string) => {
      if (val && !['development', 'staging', 'production'].includes(val)) {
        envVarError(envVar, 'Unknown environment name.')
      }
      return val
    }
  } else if (envVar === 'PORT') {
    return (val?: string) => {
      if (val) {
        const parsed = parseInt(val, 10)
        if (Number.isNaN(parsed)) {
          return undefined
        }
        return parsed
      }
      return undefined
    }
  } else if (envVar === 'DIAGNOSTICS_PORT') {
    return (val?: string) => {
      if (val) {
        const parsed = parseInt(val, 10)
        if (Number.isNaN(parsed)) {
          return undefined
        }
        return parsed
      }
      return undefined
    }
  } else if (envVar.startsWith('API_ENDPOINT_')) {
    return (val?: string) => {
      if (!val || !/^https?:\/\/[a-z0-9]/.test(val)) {
        envVarError(envVar, 'Illegal value')
      }
      return val
    }
  } else if (envVar === 'DEV_MODE_DISABLE_AUTH') {
    return (val?: string) => {
      if (val) {
        try {
          return Boolean(JSON.parse(val))
        } catch {
          return false
        }
      } else {
        return false
      }
    }
  }
}

const getEnvVar = (envVar: string, required = false) => {
  const rawVal = process.env[envVar]

  if (!rawVal && required) {
    envVarError(envVar)
  }

  const envVarProcessor = getEnvVarProcessor(envVar)
  if (envVarProcessor) {
    return envVarProcessor(rawVal)
  }

  return rawVal
}

export default {
  env: getEnvVar('NODE_ENV', true) as AppEnvironment,
  httpServer: {
    port: (getEnvVar('PORT') as number) || 4000,
    corsAllowedOrigin: getEnvVar('CORS_ALLOWED_ORIGIN', true) as string,
  },
  db: {
    connectionString: getEnvVar('PG_CONNECTION_STRING', true) as string,
  },
}
