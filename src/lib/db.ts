import postgres from 'postgres'
import { Logger } from 'pino'

import config from './config'
import logger from './logger'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let sql: postgres.Sql<any>

const connect = async (): Promise<void> => {
  logger.info('Initializing connection to Postgres...')
  sql = postgres(config.db.connectionString, {
    max: 4, // max number of connections
    connect_timeout: 5, // connect timeout in seconds
    // idle_timeout: 0,
  })
  logger.info('Connection to Postgres initialized.')
}

const disconnect = async (finalLogger?: Logger): Promise<void> => {
  const loggerInstance = finalLogger || logger
  loggerInstance.info('Closing connection to Postgres...')
  await sql.end()
  loggerInstance.info('Connection to Postgres closed.')
}

export { sql }

export default { connect, disconnect }
