import { Server } from 'http'
import express from 'express'
import cors from 'cors'
import compression from 'compression'
import pinoHttp from 'pino-http'
import { Logger } from 'pino'

import config from './config'
import logger from './logger'
import routes from '../routes'

const port = config.httpServer.port
let server: Server
let started = false

const allowedOrigins: (string | RegExp)[] = config.httpServer.corsAllowedOrigin.split(',')
if (config.env === 'development') {
  allowedOrigins.push(/https?:\/\/[a-z0-9]+.ngrok.io/)
}

const start = (): Promise<void> => {
  const app = express()

  app.use(
    cors({
      credentials: true,
      origin: allowedOrigins,
    }),
  )
  app.use(express.urlencoded({ extended: false }))
  app.use(express.json())
  app.use(compression())
  app.use(
    pinoHttp({
      logger,
    }),
  )

  app.use('/rest', routes)

  return new Promise((resolve, reject) => {
    logger.info('Starting HTTP server...')
    server = app.listen(port, () => {
      logger.info(`HTTP server started @ http://localhost:${port}.`)
      started = true
      resolve()
    })
    server.on('error', (err) => {
      logger.error(`Could not start HTTP server on port ${port}.`)
      reject(err)
    })
  })
}

const stop = (finalLogger?: Logger): Promise<void> => {
  if (!started) {
    throw new Error('HTTP server not started.')
  }
  const loggerInstance = finalLogger || logger
  return new Promise((resolve, reject) => {
    loggerInstance.info('Shutting down HTTP server...')
    server.close((err) => {
      if (err) {
        reject(err)
      }
      started = false
      loggerInstance.info('HTTP server is now down.')
      resolve()
    })
  })
}

export default { start, stop }
