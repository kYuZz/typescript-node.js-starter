import pino from 'pino'
import pinoColada from 'pino-colada'

import config from './config'

const devOptions = {
  level: 'debug',
  prettyPrint: {},
  prettifier: pinoColada,
}

const options = {}

const logger = pino(config.env === 'development' ? devOptions : options).child({ env: config.env })
// const logger = pino(options).child({ env: config.env })

export default logger
