export const on = <T>(promise: Promise<T>): Promise<[Error, void] | [void, T]> =>
  promise.then((res) => [undefined, res] as [void, T]).catch((e: Error) => [e, undefined] as [Error, void])
