import { Router } from 'express'

import widgetsRouter from './widgets'

const router = Router()

router.use('/widgets', widgetsRouter)

export default router
