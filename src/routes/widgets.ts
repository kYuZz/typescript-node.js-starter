import { Request, Response, Router } from 'express'
import { StatusCode } from 'status-code-enum'

import { on } from '../lib/utils'

import { listWidgets, findWidget } from '../entities/widget'

const { ServerErrorInternal, SuccessOK, ClientErrorNotFound } = StatusCode

const router = Router()

router.get('/', async (_: Request, res: Response): Promise<Response> => {
  const [err, widgets] = await on(listWidgets())
  if (err) {
    return res.status(ServerErrorInternal).send()
  }
  return res.status(SuccessOK).json(widgets)
})

type FindWidgetReqParams = {
  id: string
}

router.get('/:id', async (req: Request<FindWidgetReqParams>, res: Response): Promise<Response> => {
  const { id } = req.params
  const [err, contract] = await on(findWidget(id))
  if (err) {
    return res.status(ServerErrorInternal).send()
  }
  if (contract) {
    return res.status(SuccessOK).json(contract)
  }
  return res.status(ClientErrorNotFound).send()
})

export default router
